$(function () {
    // 获取数据源生成树形
    $('#navList').kendoTreeView({
        dataSource: [{
            id: '00',
            pId: 'root',
            text: '<i class="fas fa-home"></i><abbr><span>首页</span><small>Home</small></abbr>',
            encoded: false,
            expanded: true,
            directory: '/',
            location: 'home',
            items: editData(navData)
        }],
        template:
            '# var node = JSON.stringify(item).replace(/\\"/g, "\'"); #' +
            '<a class="k-link" href="javascript:editTree(#= node #);">#= item.text #</a>',
        dragAndDrop: true,
        drop: function (e) {
            var source = this.dataItem(e.sourceNode),
                destination = this.dataItem(e.destinationNode);
            if (e.valid) {
                $.fn.ajaxPost({
                    ajaxData: {
                        'sourceId': source.id,
                        'destinationId': destination.id,
                        'dropPosition': e.dropPosition
                    },
                    isMsg: true
                });
            }
        }
    });
});

// 数据处理
function editData(data) {
    $.each(data, function (i, item) {
        if (item.items) {
            editData(item.items);
        } else {
            if (item.url) {
                item.directory = item.url.split("\"")[1];
                item.location = item.url.split("\"")[3];
                delete item.cssClass;
                delete item.url;
            }
        }
    });
    return data;
}

// 增删改查
function editTree(node) {
    var data = {};
    data.id = node.id;
    data.parentId = node.pId;
    data.sort = node.index;
    data.name = '';
    data.subName = '';
    data.icon = '';
    data.dot = 0;
    data.badge = '';
    node.directory ? data.directory = node.directory : data.directory = '';
    node.location ? data.location = node.location : data.location = '';
    node.expanded ? data.expanded = 1 : data.expanded = 0;
    if (node.pId === 'root' || node.pId === '0') {
        $.each($(node.text), function (i, items) {
            if ($(items)[0].localName === 'i') {
                data.icon = $(items)[0].className;
            }
            if ($(items)[0].localName === 'sup') {
                data.dot = 1;
            }
            if ($(items)[0].localName === 'abbr') {
                $.each($(items).children(), function (i, items) {
                    if ($(items)[0].localName === 'span') {
                        data.name = $(items).text();
                    }
                    if ($(items)[0].localName === 'small') {
                        data.subName = $(items).text();
                    }
                    if ($(items)[0].localName === 'sub') {
                        data.badge = badgeCheck($(items).text());
                    }
                });
            }
        });
    } else {
        $.each($(node.text), function (i, items) {
            if ($(items)[0].localName === 'i') {
                data.icon = $(items)[0].className;
            }
            if ($(items)[0].localName === 'span') {
                data.name = $(items).text();
            }
            if ($(items)[0].localName === 'small') {
                data.subName = $(items).text();
            }
            if ($(items)[0].localName === 'sub') {
                data.badge = badgeCheck($(items).text());
            }
        });
    }
    $('#navEdit').html(kendo.template($('#editTemplate').html())(data));
}

// 增
function createTree() {
    $('#viewArea').hide();
    $('#editArea').hide();
    $('#addArea').show();
}

// 增保存
function saveCreateTree() {
    if ($('#addArea form').kendoValidator().data('kendoValidator').validate()) {
        $.fn.ajaxPost({
            ajaxData: $('#addArea form').serializeObject(),
            succeed: function () {
                location.reload();
            },
            isMsg: true
        });
    }
}

// 删
function destroyTree(id, text) {
    confirmMsg('删除确认', '你确定要删除导航<strong class="theme-m mx-1">' + text + '</strong>吗？', 'question', function () {
        $.fn.ajaxPost({
            ajaxData: {
                'id': id
            },
            succeed: function () {
                location.reload();
            },
            isMsg: true
        });
    });
}

// 改
function updateTree() {
    $('#viewArea').hide();
    $('#addArea').hide();
    $('#editArea').show();
}

// 改保存
function saveUpdateTree() {
    if ($('#editArea form').kendoValidator().data('kendoValidator').validate()) {
        $.fn.ajaxPost({
            ajaxData: $('#editArea form').serializeObject(),
            succeed: function () {
                location.reload();
            },
            isMsg: true
        });
    }
}

// 取消
function cancelTree() {
    $('#addArea').hide();
    $('#editArea').hide();
    $('#viewArea').show();
}

// 图标预览
function iconShow(dom) {
    $(dom).prev().removeClass().addClass($(dom).val());
}

// 徽章判断
function badgeCheck(badge) {
    if (badge === '\uD83D\uDD25') {
        return 'hot';
    } else if (badge === '\u2714\uFE0F') {
        return 'done';
    } else if (badge === '\u2728') {
        return 'update';
    } else if (badge === '\uD83C\uDF1F') {
        return 'new';
    } else if (badge === '\u23F3') {
        return 'ing';
    } else if (badge === '\uD83C\uDF31') {
        return 'work';
    } else if (badge === '\u231A') {
        return 'wait';
    } else {
        return badge;
    }
}

// 徽章预览
function badgeShow(dom) {
    if ($(dom).val() !== checkBadge($(dom).val())) {
        $(dom).prev().children().text(checkBadge($(dom).val()));
    } else {
        $(dom).prev().children().text('');
    }
}